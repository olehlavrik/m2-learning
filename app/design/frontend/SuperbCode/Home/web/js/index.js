/**
 * Created by olehlavryk on 10.09.17.
 */
requirejs(['jquery', 'jquery.bootstrap'], function (jQuery, jQueryBootstrap) {
    jQuery('.carousel').carousel();

    jQuery('li.parent').hover(function() {
        jQuery(this).find('ul:first').stop(true, true).delay(200).fadeIn(500);
    }, function() {
        jQuery(this).find('ul').stop(true, true).delay(200).fadeOut(500);
    });

});